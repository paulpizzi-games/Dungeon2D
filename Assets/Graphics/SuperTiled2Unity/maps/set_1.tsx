<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.2.1" name="set_1" tilewidth="128" tileheight="128" tilecount="4" columns="0">
 <grid orientation="orthogonal" width="1" height="1"/>
 <tile id="0">
  <image width="128" height="128" source="rock_wall3.png"/>
 </tile>
 <tile id="1">
  <image width="128" height="128" source="rock_wall4.png"/>
 </tile>
 <tile id="2">
  <image width="128" height="128" source="tile_wall4@2x.png"/>
 </tile>
 <tile id="3">
  <image width="128" height="128" source="tile_wall3@2x.png"/>
 </tile>
</tileset>
