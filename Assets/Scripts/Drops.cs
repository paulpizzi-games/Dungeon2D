﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Drops : MonoBehaviour
{
    // public member vars
    public GameObject drop;

    // private member vars
    private PlayerController player;

    // Start is called before the first frame update
    void Start()
    {
        player = FindObjectOfType<PlayerController>();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.tag == "Player")
        {
            Debug.Log("collision");
            float newHp = player.health.GetHp() + 25f;
            if (newHp > 100) newHp = 100;
            player.health.SetHealth(newHp);
            drop.SetActive(false);
            //Destroy(this);
        }
    }
}