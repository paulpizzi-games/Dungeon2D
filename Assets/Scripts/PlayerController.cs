﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    // public member vars
    public float speed;
    public Animator anim;
    [HideInInspector]
    public HealthBar health;

    // private member vars
    private bool facing = true; // false-left true-right
    private GameOver gameover;

    //sounds
    public AudioManager audioManager;
   // public AudioClip attackSound;
   // public AudioClip hurtSound;
   // public AudioSource audioController;

    // Start is called before the first frame update
    void Start()
    {
        health = FindObjectOfType<HealthBar>();
        gameover = FindObjectOfType<GameOver>();
    }

    public void Move(float direction)
    {

        // move up
        if (direction == 1 || Input.GetKey(KeyCode.W))
        {
            audioManager.Play("footsteps");
            GetComponent<Rigidbody2D>().velocity = new Vector2(0, speed);
            anim.SetTrigger("playerWalk");
            if (!facing)
            {
                transform.localRotation = Quaternion.Euler(0, 0, 0);
                facing = true;
            }
        }
        // move left
        else if (direction == -2 || Input.GetKey(KeyCode.A))
        {
            audioManager.Play("footsteps");
            GetComponent<Rigidbody2D>().velocity = new Vector2(-speed, 0);
            anim.SetTrigger("playerWalk");
            if (facing)
            {
                transform.localRotation = Quaternion.Euler(0, 180, 0);
                facing = false;
            }
        }
        // move down
        else if (direction == -1 || Input.GetKey(KeyCode.S))
        {
            audioManager.Play("footsteps");
            GetComponent<Rigidbody2D>().velocity = new Vector2(0, -speed);
            anim.SetTrigger("playerWalk");
            if (facing)
            {
                transform.localRotation = Quaternion.Euler(0, 180, 0);
                facing = false;
            }
        }
        // move right
        else if (direction == 2 || Input.GetKey(KeyCode.D))
        {
            audioManager.Play("footsteps");
            GetComponent<Rigidbody2D>().velocity = new Vector2(speed, 0);
            anim.SetTrigger("playerWalk");
            if (!facing)
            {
                transform.localRotation = Quaternion.Euler(0, 0, 0);
                facing = true;
            }
        }
        // stand still
        else
        {
            GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
            anim.SetTrigger("playerIdle");
        }
    }

    public void Attack()
    {
        anim.SetTrigger("playerAttack");
        audioManager.Play("swordSwing");
        anim.SetTrigger("playerIdle");
    }

    public void TakeDamage(float dmg)
    {
        anim.SetTrigger("playerHit");

        health.TakeDamage(dmg);

        if (health.GetHp() <= 0)
        {
            //GameOver.dead = true;
            anim.SetTrigger("playerDie");
            anim.SetTrigger("playerDead");
            audioManager.Play("gameover");
            gameover.OpenMenu();
        }
    }
}
