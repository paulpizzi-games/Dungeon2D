﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VirtualController : MonoBehaviour
{

    public PlayerController player;
    private AudioManager audioManager;

    // Start is called before the first frame update
    void Start()
    {
        player = FindObjectOfType<PlayerController>();
        audioManager = FindObjectOfType<AudioManager>();
        
    }

    // Play Movement
    public void DPadLeft()
    {
        player.Move(-2); // move left
    }

    public void DPadRight()
    {
        player.Move(2); // move right
    }

    public void DPadDown()
    {
        player.Move(-1); // move down
    }

    public void DPadUp()
    {
        player.Move(1); // move up
    }

    public void DPadUnpressed()
    {
        player.Move(0); // stand still
        audioManager.Stop("footsteps");
    }

    // Player Attack
    public void ButtonAttack()
    {
        player.Attack();
    }

    // Pause Menu
    public void PauseMenu()
    {
        global::PauseMenu.pauseCommand = true;
    }
}