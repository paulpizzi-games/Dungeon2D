﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthBar : MonoBehaviour
{
    // private member vars

    private Transform bar;
    private AudioManager audioManager;

    // Start is called before the first frame update
    void Start()
    {
        bar = transform.Find("bar");
        audioManager = FindObjectOfType<AudioManager>();
    }

    public float GetHp()
    {
        return bar.localScale.x * 100;
    }

    public void SetHealth(float health)
    {
        health = health / 100;
        if (GetHp() <= 0)
        {
            //Debug.Log("DEAD BOY");
        } else
        {
            bar.localScale = new Vector3(health, 1);
        }
        
    }

    public void TakeDamage(float dmg)
    {
        audioManager.Play("playerHit");
        audioManager.Play("swordHit");
        float dmgF = dmg;

        float cur = GetHp();

        SetHealth(cur - dmgF);
    }

}