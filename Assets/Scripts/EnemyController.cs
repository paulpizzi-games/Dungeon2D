﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class EnemyController : MonoBehaviour
{
    // public member vars
    public float speed;
    public Animator anim;
    public GameObject drop;
    public GameObject enemy;
    public bool facing = true; // true = right ; false = left; 

    // private member vars
    private Transform player;
    private Rigidbody2D rb;
    private int health = 100;
    private AudioManager audioManager;
    
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        audioManager = FindObjectOfType<AudioManager>();
        rb = GetComponent<Rigidbody2D>();
        speed = speed / 50;
    }

    public void Attack()
    {
        rb.velocity = new Vector2(0, 0);
        anim.SetTrigger("orkAttack");
        if (!anim.GetCurrentAnimatorStateInfo(0).IsName("orkAttack"))
        {
            anim.SetTrigger("orkIdle");
        }
    }

    public void GetHit()
    {
        
        anim.SetTrigger("orkHit");
        audioManager.Play("orkHit");
        audioManager.Play("swordHit");
        health -= 1;
        if (health == 0) //dying
        {
            // INSERT DYING SOUND HERE
            anim.SetTrigger("orkDie");
            anim.SetTrigger("orkDead");
            Debug.Log(GameController.enemies.Count);
            rb.isKinematic = true;
            rb.freezeRotation = true;

            if (!anim.GetCurrentAnimatorStateInfo(0).IsName("orkDie"))
            {
                GameController.enemies.Remove(this);
                enemy.SetActive(false);
                Destroy(this);
                Debug.Log(GameController.enemies.Count);
            }
        }
    }

    public void Move()
    {
        int xDir = 0;
        int yDir = 0;
        // If the player and enemy are too far away the enemy won't start attacking
        if (Math.Abs(player.position.x - transform.position.x) > 10 || Math.Abs(player.position.y - transform.position.y) > 10)
        {
            rb.velocity = new Vector2(0, 0);
            anim.SetTrigger("orkIdle");
            return;
        }

        //Check if players y position is greater than enemy's y position, then set y direction 1 (move up), else set it to -1 (move down).
        if (player.position.y >= transform.position.y) yDir = 1;
        else yDir = -1;

        //Check if players x position is greater than enemy's x position, then set x direction to -1 (move left), else set to 1 (move right).
        if (player.position.x >= transform.position.x) xDir = 1;
        else xDir = -1;
        
        // If the Player is dead stop moving
        if (GameObject.FindObjectOfType<PlayerController>().health.GetHp() <= 0)
        {
            if (xDir == -1 || xDir == 1) xDir = 0;
            if (yDir == -1 || yDir == 1) yDir = 0;
        }

        if (yDir == 1) //up
        {
            audioManager.Play("footsteps");
            transform.Translate(0, speed, 0);
            anim.SetTrigger("orkWalk");

        } else if (yDir == -1) //down
        {
            audioManager.Play("footsteps");
            transform.Translate(0, -speed, 0);
            anim.SetTrigger("orkWalk");
        }
        if (xDir == 1) //right
        {
            audioManager.Play("footsteps");
            if (!facing)
            {
                transform.localRotation = Quaternion.Euler(0, 0, 0);
                facing = true;
            }
            transform.Translate(speed, 0, 0);
            anim.SetTrigger("orkWalk");
        } else if (xDir == -1) //left
        {
            audioManager.Play("footsteps");
            if (facing)
            {
                transform.localRotation = Quaternion.Euler(0, 180, 0);
                facing = false;
            }
            transform.Translate(speed, 0, 0);
            anim.SetTrigger("orkWalk");
        } else
        {
            audioManager.Stop("footsteps");
            rb.velocity = new Vector2(0, 0);
            anim.SetTrigger("orkIdle");
        }
        audioManager.Stop("footsteps");
        transform.Translate(0, 0, 0);
    }

    private void OnDestroy()
    {
        System.Random rnd = new System.Random();
        if (rnd.Next(101) <= 50)
        {
            if (drop != null)
            {
                GameObject dropped = Instantiate(drop, transform.position, drop.transform.rotation);
                if (dropped != null) dropped.SetActive(true);
            }
        }
    }
}