﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOver : MonoBehaviour
{

    public static bool dead = false;
    public GameObject gameOverMenu;
    AudioManager audioManager;

    // Start is called before the first frame update
    void Start()
    {

        gameOverMenu.SetActive(false);
        Time.timeScale = 1f;
        audioManager = FindObjectOfType<AudioManager>();
    }

    // Update is called once per frame
    void Update()
    {
        if (dead)
        {
            OpenMenu();
        }
    }

    public void OpenMenu()
    {
        gameOverMenu.SetActive(true);
        audioManager.StopAll();
       // Time.timeScale = 0f;
        dead = false;
    }

    public void Restart()
    {
        dead = false;
        Time.timeScale = 1f;
        gameOverMenu.SetActive(false);
        SceneManager.LoadScene("Game");
    }

    public void Menu()
    {
        gameOverMenu.SetActive(false);
        SceneManager.LoadScene("MainMenu");
    }

    public void Quit()
    {
        Application.Quit();
    }
}