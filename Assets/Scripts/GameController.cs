﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class GameController : MonoBehaviour
{
    // private member vars
    private PlayerController player;
    public static List<EnemyController> enemies;
    

    // Start is called before the first frame update
    void Start()
    {
        player = FindObjectOfType<PlayerController>();
        enemies = new List<EnemyController>();
        foreach (EnemyController enemy in FindObjectsOfType<EnemyController>())
        {
            enemies.Add(enemy);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
        if (enemies != null && player != null)
        {
          //  Debug.Log(enemies.Count);
            if (enemies.Count != 0)
            {
                foreach (EnemyController enemy in enemies)
                {
                    enemy.Move();

                    // If the distance is low enough the enemy starts attacking
                    if (Mathf.Abs((player.transform.position.x - enemy.transform.position.x)) < 3.2
                        && Mathf.Abs((player.transform.position.y - enemy.transform.position.y)) < 1)
                    {
                        if (!enemy.anim.GetCurrentAnimatorStateInfo(0).IsName("orkAttack"))
                        {
                            if (player.health.GetHp() > 0) enemy.Attack();
                        }
                        else
                        {
                            player.TakeDamage(0.5f);
                        }

                        if (player.anim.GetCurrentAnimatorStateInfo(0).IsName("playerAttack"))
                        {
                            enemy.GetHit();
                        }
                    }
                }
            }
        }
    }
}