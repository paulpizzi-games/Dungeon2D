﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class AudioManager : MonoBehaviour
{

    public Sound[] sounds;

    // Start is called before the first frame update
    void Awake()
    {
        foreach (Sound s in sounds) {
            s.source = gameObject.AddComponent<AudioSource>();
            s.source.clip = s.audio;
            s.source.volume = s.volume;
        }
        Play("mainTheme");
    }

    // Update is called once per frame
    public void Play(string name)
    {
   
        Sound s = Array.Find(sounds, Sound => Sound.name == name);
        if (s != null)
        {
            //Debug.Log(name);
            //s.source.PlayOneShot(s.audio);
            if (!s.source.isPlaying)
            {
                s.source.Play();
            }
        }
    }

    public void Stop(string name)
    {
        Sound s = Array.Find(sounds, Sound => Sound.name == name);
        if (s != null)
        {
            s.source.Stop();
        }
    }

    public void StopAll()
    {
        Debug.Log("stopall");
        foreach(Sound s in sounds)
        {
            s.source.Stop();
        }
        Play("gameover");
    }
}